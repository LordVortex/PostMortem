# PostMortem
Post Mortem is a post-apocalyptic open world game created in Minecraft.
You are a stranded survivor and are trying to stay alive by any means necessary.
Explore a stunning world alone or with friends and use the many possibilities the world offers,
to fight off hunger, disease and death.
